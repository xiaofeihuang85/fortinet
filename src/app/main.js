import {Component} from '@angular/core';
import {Router} from '@angular/router';

export class Staff {
  name: string;
  start: number;
  startStr: string;
  end: number;
  endStr: string;
  hours: number;

  constructor(name: string, start: number, end:number) {
    this.name = name;
    this.start = start;
    this.end = end;
    this.startStr = this.getTimeStr(start);
    this.endStr = this.getTimeStr(end);
    const time = (end - start) / 100;
    this.hours = Math.floor(time) + ((time - Math.floor(time)) / 0.6);
  }

  getTimeStr(time: number): string {
    const timeNum = time / 100;
    let timeStr = '';
    if (timeNum === Math.floor(timeNum)) {
      if (timeNum < 12) {
        timeStr = Math.floor(timeNum) + ':00 AM';
      } else if (timeNum === 12) {
        timeStr = '12:00 PM';
      } else {
        timeStr = (Math.floor(timeNum) - 12) + ':00 PM';
      }
    }

    if (timeNum !== Math.floor(timeNum)) {
      if (timeNum < 12) {
        timeStr = Math.floor(timeNum) + ':' + (time - (Math.floor(timeNum) * 100)) + ' AM';
      } else {
        timeStr = (Math.floor(timeNum) - 12) + ':' + (time - (Math.floor(timeNum) * 100)) + ' PM';
      }
    }
    return timeStr;
  }

}

@Component({
  selector: 'fountain-app',
  template: require('./main.html')
})
export class MainComponent {
  staffs: Staff[];
  nameToggle: boolean;
  startToggle: boolean;
  endToggle: boolean;
  hoursToggle: boolean;
  router: Router;

  constructor(router: Router) {
    this.staffs = [
      new Staff('Josh', 1500, 1715),
      new Staff('Ben', 900, 1400),
      new Staff('Vince', 1200, 1845),
      new Staff('Jack', 800, 1700),
      new Staff('Andrew', 800, 1600),
      new Staff('Rob', 600, 1600),
      new Staff('James', 600, 1600)
    ];
    this.nameToggle = true;
    this.startToggle = true;
    this.endToggle = true;
    this.hoursToggle = true;

    const params = router.parseUrl(router.url).queryParams;
    let target = null;
    if (Object.keys(params).length !== 0) {
      for (const entry of this.staffs) {
        if (entry.name === params.name) {
          target = entry;
        }
      }
      this.delete(target);
      this.staffs.push(new Staff(params.name, params.start, params.end));
    }
  }

  delete(staff: Staff): void {
    const i = this.staffs.indexOf(staff);
    if (i !== -1) {
      this.staffs.splice(i, 1);
    }
  }

  sortByName(): void {
    if (this.nameToggle) {
      this.staffs.sort(this.compareName);
      this.nameToggle = false;
    } else {
      this.staffs.sort(this.compareNameDec);
      this.nameToggle = true;
    }
  }

  sortByStart(): void {
    if (this.startToggle) {
      this.staffs.sort(this.compareStart);
      this.startToggle = false;
    } else {
      this.staffs.sort(this.compareStartDec);
      this.startToggle = true;
    }
  }

  sortByEnd(): void {
    if (this.endToggle) {
      this.staffs.sort(this.compareEnd);
      this.endToggle = false;
    } else {
      this.staffs.sort(this.compareEndDec);
      this.endToggle = true;
    }
  }

  sortByHours(): void {
    if (this.hoursToggle) {
      this.staffs.sort(this.compareHours);
      this.hoursToggle = false;
    } else {
      this.staffs.sort(this.compareHoursDec);
      this.hoursToggle = true;
    }
  }

  compareName(a, b): number {
    if (a.name < b.name) {
      return -1;
    }
    if (a.name > b.name) {
      return 1;
    }
    return 0;
  }

  compareNameDec(a, b): number {
    if (a.name < b.name) {
      return 1;
    }
    if (a.name > b.name) {
      return -1;
    }
    return 0;
  }

  compareStart(a, b): number {
    if (a.start < b.start) {
      return -1;
    }
    if (a.start > b.start) {
      return 1;
    }
    if (a.start === b.start) {
      if (a.name < b.name) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    }
  }

  compareStartDec(a, b): number {
    if (a.start < b.start) {
      return 1;
    }
    if (a.start > b.start) {
      return -1;
    }
    if (a.start === b.start) {
      if (a.name < b.name) {
        return 1;
      }
      if (a.name > b.name) {
        return -1;
      }
      return 0;
    }
  }

  compareEnd(a, b): number {
    if (a.end < b.end) {
      return -1;
    }
    if (a.end > b.end) {
      return 1;
    }
    if (a.end === b.end) {
      if (a.name < b.name) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    }
  }

  compareEndDec(a, b): number {
    if (a.end < b.end) {
      return 1;
    }
    if (a.end > b.end) {
      return -1;
    }
    if (a.end === b.end) {
      if (a.name < b.name) {
        return 1;
      }
      if (a.name > b.name) {
        return -1;
      }
      return 0;
    }
  }

  compareHours(a, b): number {
    if (a.hours < b.hours) {
      return -1;
    }
    if (a.hours > b.hours) {
      return 1;
    }
    if (a.hours === b.hours) {
      if (a.name < b.name) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    }
  }

  compareHoursDec(a, b): number {
    if (a.hours < b.hours) {
      return 1;
    }
    if (a.hours > b.hours) {
      return -1;
    }
    if (a.hours === b.hours) {
      if (a.name < b.name) {
        return 1;
      }
      if (a.name > b.name) {
        return -1;
      }
      return 0;
    }
  }
}
