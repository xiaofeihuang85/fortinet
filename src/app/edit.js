import {Component, Input} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'edit-staff',
  template: require('./edit.html')
})
export class EditComponent {
  @Input() name;
  @Input() start;
  @Input() end;
  originStart: string;
  originEnd: string;
  router: Router;

  constructor(route:ActivatedRoute, router: Router) {
    this.router = router;
    this.name = route.snapshot.params.name;
    this.start = route.snapshot.params.start;
    this.originStart = route.snapshot.params.start;
    this.end = route.snapshot.params.end;
    this.originEnd = route.snapshot.params.end;
  }

  onSubmit(form: any): void {
    this.router.navigate(['/'], {queryParams: {name: form.name, start: form.start, end: form.end, originStart: this.originStart, originEnd: this.originEnd}});
  }
}
