import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {routing, RootComponent} from './routes';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';

import {MainComponent} from './main';
import {EditComponent} from './edit';

@NgModule({
  imports: [
    BrowserModule,
    routing,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    RootComponent,
    MainComponent,
    EditComponent
  ],
  bootstrap: [RootComponent]
})
export class AppModule {}
