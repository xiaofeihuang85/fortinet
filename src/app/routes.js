import {Component} from '@angular/core';
import {RouterModule} from '@angular/router';
import {MainComponent} from './main';
import {EditComponent} from './edit';

@Component({
  selector: 'fountain-root',
  template: '<router-outlet></router-outlet>'
})
export class RootComponent {}

export const routes = [
  {
    path: '',
    component: MainComponent
  },
  {
    path: 'edit',
    component: EditComponent
  }
];

export const routing = RouterModule.forRoot(routes);
